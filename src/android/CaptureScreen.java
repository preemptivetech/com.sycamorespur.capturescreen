/**
 * Copyright (C) 2012 30ideas (http://30ide.as)
 * MIT licensed
 *
 * @author Josemando Sobral
 * @created Jul 2nd, 2012.
 * improved by Hongbo LU
 */
package com.sycamorespur.capturescreen;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class CaptureScreen extends CordovaPlugin {
    private CallbackContext mCallbackContext;
    private String mAction;
    private JSONArray mArgs;


    private String mFormat;
    private String mFileName;
    private Integer mQuality;
	
	private Integer mX = 0;
	private Integer mY = 0;
	private Integer mWidth = 100;
	private Integer mHeight = 100;

    protected final static String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public static final int PERMISSION_DENIED_ERROR = 20;
    public static final int SAVE_SCREENSHOT_SEC = 0;
    public static final int SAVE_SCREENSHOT_URI_SEC = 1;

    @Override
    public Object onMessage(String id, Object data) {
        if (id.equals("onGotXWalkBitmap")) {
            Bitmap bitmap = (Bitmap) data;
            if (bitmap != null) {
				Bitmap bmpCropped = CropBitmap(bitmap);
				
                if (mAction.equals("saveScreenshot")) {
                    saveScreenshot(bmpCropped, mFormat, mFileName, mQuality);
                } else if (mAction.equals("getScreenshotAsURI")) {
                    getScreenshotAsURI(bmpCropped, mQuality);
                }
            }
        }
        return null;
    }
	
	private Bitmap CropBitmap(Bitmap fullScreenShot) {
		
		String sError = "";
		
		try {
			
			if(fullScreenShot != null) {
				//int x = 10; //v1.getWidth()/10;
				//int y = 50; //v1.getHeight()/10
				//int width = 300; //bitmap.getWidth() - x;
				//int height = 200; //bitmap.getHeight() - y;
				
				DisplayMetrics dm = new DisplayMetrics();
				this.cordova.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
				double dDensity = (double)dm.density;  //getResources().getDisplayMetrics().density;
				
				sError += " density: " + dDensity;
				sError += " x: " + mX;
				sError += " y: " + mY;
				sError += " width: " + mWidth;
				sError += " height: " + mHeight;
				sError += " b.width: " + fullScreenShot.getWidth();
				sError += " b.height: " + fullScreenShot.getHeight(); 
				sError += " b.density: " + fullScreenShot.getDensity();
				
				Bitmap croppedBitmap = Bitmap.createBitmap(fullScreenShot, 
				(int)Math.round((int)mX * dDensity), 
				(int)Math.round((int)mY * dDensity), 
				(int)Math.round((int)mWidth * dDensity), 
				(int)Math.round((int)mHeight * dDensity));
				return croppedBitmap;
			}
			
		} catch (Exception e) {
            mCallbackContext.error(e.getMessage() + sError);
        }
		
		return null;
	}

    private Bitmap getBitmap() {
        Bitmap bitmap = null;

        boolean isCrosswalk = false;
        try {
			Class.forName("org.crosswalk.engine.XWalkWebViewEngine");
			isCrosswalk = true;
        } catch (Exception e) {
        }

        if (isCrosswalk) {
            webView.getPluginManager().postMessage("captureXWalkBitmap", this);
        } else {
            View view = webView.getView();//.getRootView();
            view.setDrawingCacheEnabled(true);
            bitmap = Bitmap.createBitmap(view.getDrawingCache());
            view.setDrawingCacheEnabled(false);
        }
		
		return CropBitmap(bitmap);

        //return bitmap;
    }

    private void scanPhoto(String imageFileName) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(imageFileName);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.cordova.getActivity().sendBroadcast(mediaScanIntent);
    }

    private void saveScreenshot(Bitmap bitmap, String format, String fileName, Integer quality) {
        try {
            //File folder = new File(Environment.getExternalStorageDirectory(), "Pictures"); 				// this one gets the common phone directory, so all apps can use pictures put here
			File folder = new File(webView.getContext().getExternalFilesDir(null), "DaddyUpTempPics");		// this one gets a uses a directory in the app cache (not available to whole phone)
            if (!folder.exists()) {
                folder.mkdirs();
            }

            File f = new File(folder, fileName + "." + format);

            FileOutputStream fos = new FileOutputStream(f);
            if (format.equals("png")) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            } else if (format.equals("jpg")) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality == null ? 100 : quality, fos);
            }
            JSONObject jsonRes = new JSONObject();
            jsonRes.put("filePath", f.getAbsolutePath());
            PluginResult result = new PluginResult(PluginResult.Status.OK, jsonRes);
            mCallbackContext.sendPluginResult(result);

            scanPhoto(f.getAbsolutePath());
            fos.close();
        } catch (JSONException e) {
            mCallbackContext.error(e.getMessage());

        } catch (IOException e) {
            mCallbackContext.error(e.getMessage());

        }
    }

    private void getScreenshotAsURI(Bitmap bitmap, int quality) {
        try {
            ByteArrayOutputStream jpeg_data = new ByteArrayOutputStream();

            if (bitmap.compress(CompressFormat.JPEG, quality, jpeg_data)) {
                byte[] code = jpeg_data.toByteArray();
                byte[] output = Base64.encode(code, Base64.NO_WRAP);
                String js_out = new String(output);
                js_out = "data:image/jpeg;base64," + js_out;
                JSONObject jsonRes = new JSONObject();
                jsonRes.put("URI", js_out);
                PluginResult result = new PluginResult(PluginResult.Status.OK, jsonRes);
                mCallbackContext.sendPluginResult(result);

                js_out = null;
                output = null;
                code = null;
            }

            jpeg_data = null;

        } catch (JSONException e) {
            mCallbackContext.error(e.getMessage());

        } catch (Exception e) {
            mCallbackContext.error(e.getMessage());

        }
    }

    public void saveScreenshot() throws JSONException{
        mFormat = (String) mArgs.get(0);
        mQuality = (Integer) mArgs.get(1);
        mFileName = (String) mArgs.get(2);
		mX = (Integer) mArgs.get(3);
		mY = (Integer) mArgs.get(4);
		mWidth = (Integer) mArgs.get(5);
		mHeight = (Integer) mArgs.get(6);

        super.cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mFormat.equals("png") || mFormat.equals("jpg")) {
                    Bitmap bitmap = getBitmap();
                    if (bitmap != null) {
                        saveScreenshot(bitmap, mFormat, mFileName, mQuality);
                    }
                } else {
                    mCallbackContext.error("format " + mFormat + " not found");

                }
            }
        });
    }

    public void getScreenshotAsURI() throws JSONException{
        mQuality = (Integer) mArgs.get(0);
		mX = (Integer) mArgs.get(1);
		mY = (Integer) mArgs.get(2);
		mWidth = (Integer) mArgs.get(3);
		mHeight = (Integer) mArgs.get(4);

        super.cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = getBitmap();
                if (bitmap != null) {
                    getScreenshotAsURI(bitmap, mQuality);
                }
            }
        });
    }


    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        // starting on ICS, some WebView methods
        // can only be called on UI threads
        mCallbackContext = callbackContext;
        mAction = action;
        mArgs = args;

        if (action.equals("saveScreenshot")) {
            if(PermissionHelper.hasPermission(this, PERMISSIONS[0])) {
                saveScreenshot();
            } else {
                PermissionHelper.requestPermissions(this, SAVE_SCREENSHOT_SEC, PERMISSIONS);
            }
            return true;
        } else if (action.equals("getScreenshotAsURI")) {
            if(PermissionHelper.hasPermission(this, PERMISSIONS[0])) {
                getScreenshotAsURI();
            } else {
                PermissionHelper.requestPermissions(this, SAVE_SCREENSHOT_URI_SEC, PERMISSIONS);
            }
            return true;
        }
        callbackContext.error("action not found");
        return false;
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions,
                                          int[] grantResults) throws JSONException
    {
        for(int r:grantResults)
        {
            if(r == PackageManager.PERMISSION_DENIED)
            {
                mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, PERMISSION_DENIED_ERROR));
                return;
            }
        }
        switch(requestCode)
        {
            case SAVE_SCREENSHOT_SEC:
                saveScreenshot();
                break;
            case SAVE_SCREENSHOT_URI_SEC:
                getScreenshotAsURI();
                break;
        }
    }


}
