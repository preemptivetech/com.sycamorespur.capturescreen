//
// CaptureScreen.h
//
// Created by Simon Madine on 29/04/2010.
// Copyright 2010 The Angry Robot Zombie Factory.
// - Converted to Cordova 1.6.1 by Josemando Sobral.
// MIT licensed
//
// Modifications to support orientation change by @ffd8
//

#import <Cordova/CDV.h>
#import "CaptureScreen.h"

@implementation CaptureScreen

@synthesize webView;

-(UIImage *)cropImage:(UIImage *)image rect:(CGRect)cropRect
{
   CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
   UIImage *imgC = [UIImage imageWithCGImage:imageRef]; 
   CGImageRelease(imageRef);
   return imgC;
}

- (UIImage *)getScreenshot
{
	UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
	CGRect rect = [keyWindow bounds];
	UIGraphicsBeginImageContextWithOptions(rect.size, YES, 0);
	[keyWindow drawViewHierarchyInRect:keyWindow.bounds afterScreenUpdates:NO];
	UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return img;
}

- (void)saveScreenshot:(CDVInvokedUrlCommand*)command
{
	NSNumber *dimHeight = [command.arguments objectAtIndex:6];
	NSNumber *dimWidth = [command.arguments objectAtIndex:5];
	NSNumber *dimY = [command.arguments objectAtIndex:4];
	NSNumber *dimX = [command.arguments objectAtIndex:3];
	NSString *filename = [command.arguments objectAtIndex:2];
	NSNumber *quality = [command.arguments objectAtIndex:1];

	NSString *path = [NSString stringWithFormat:@"%@.jpg",filename];
	NSString *jpgPath = [NSTemporaryDirectory() stringByAppendingPathComponent:path];

	UIImage *fullImage = [self getScreenshot];
	
	// the dimension values passed in are in pixels (from the javascript),
	// so we need to scale them appropriately (retina displays pack more pixels in one pixel...)
	CGFloat screenScale = [UIScreen mainScreen].scale;
	float fX = [dimX floatValue] * screenScale;
	float fY = [dimY floatValue] * screenScale;
	float fWidth = [dimWidth floatValue] * screenScale;
	float fHeight = [dimHeight floatValue] * screenScale;
	
	UIImage *croppedImage = [self cropImage:fullImage rect:CGRectMake(fX,fY,fWidth,fHeight)];
	
	NSData *imageData = UIImageJPEGRepresentation(croppedImage,[quality floatValue]);
	[imageData writeToFile:jpgPath atomically:NO];

	CDVPluginResult* pluginResult = nil;
	NSDictionary *jsonObj = [ [NSDictionary alloc]
		initWithObjectsAndKeys :
		jpgPath, @"filePath",
		@"true", @"success",
		nil
	];

	pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:jsonObj];
	NSString* callbackId = command.callbackId;
	[self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}

- (void) getScreenshotAsURI:(CDVInvokedUrlCommand*)command
{
	NSNumber *quality = command.arguments[0];
	UIImage *image = [self getScreenshot];
	NSData *imageData = UIImageJPEGRepresentation(image,[quality floatValue]);
	NSString *base64Encoded = [imageData base64EncodedStringWithOptions:0];
	NSDictionary *jsonObj = @{
	    @"URI" : [NSString stringWithFormat:@"data:image/jpeg;base64,%@", base64Encoded]
	};
	CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:jsonObj];
	[self.commandDelegate sendPluginResult:pluginResult callbackId:[command callbackId]];
}
@end
